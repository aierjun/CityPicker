package com.lljjcoder;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ani_aierJun on 2018/2/10.
 */

public class DataLoad {
    //把assets目录下的db文件复制到dbpath下
    private static SQLiteDatabase sqLiteDatabase;

    public static SQLiteDatabase DBCopyLoadManager(Context mContext, String dbName) {
        String dbPath = "/data/data/" + mContext.getPackageName() + "/databases/";
        File file = new File(dbPath);
        if (!new File(dbPath).exists()) {
            Log.e("Ani", "文件路径不存在!" + file.toString());
            if (!file.exists())
                file.mkdirs();
            Log.e("Ani", "创建文件路径!" + file.toString());
        }
        if (!file.exists()) {
            Log.e("Ani", "文件路径创建失败!" + file.toString());
            return null;
        }
        try {
            if (!new File(dbPath + dbName).exists())
                new File(dbPath + dbName).createNewFile();
            FileOutputStream out = new FileOutputStream(dbPath + dbName);
            InputStream in = mContext.getAssets().open(dbName);
            byte[] buffer = new byte[1024];
            int readBytes = 0;
            while ((readBytes = in.read(buffer)) != -1)
                out.write(buffer, 0, readBytes);
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SQLiteDatabase.openOrCreateDatabase(dbPath + dbName, null);
    }

    /*
    * 读取省份
    *
    * */
    public static List<String> getProvincetoData(Context context) {
        List<String> stringList = new ArrayList<>();
        if (sqLiteDatabase == null)
            sqLiteDatabase = DataLoad.DBCopyLoadManager(context.getApplicationContext(), "city.db");
        String sql = "SELECT city FROM sl_city WHERE display = 3;";
        Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            String s1 = cursor.getString(cursor.getColumnIndex("city"));
            stringList.add(s1);
//            Log.d("Ani", s1 + "   s1");
            while (cursor.moveToNext()) {
                String s2 = cursor.getString(cursor.getColumnIndex("city"));
//                Log.d("Ani", s2 + "   s2");
                stringList.add(s2);
            }
        }
        return stringList;
    }

    /*
   * 读取市级
   *
   * */
    public static List<String> getCitytoData(Context context, String provinceto) {
        List<String> stringList = new ArrayList<>();
        if (sqLiteDatabase == null)
            sqLiteDatabase = DataLoad.DBCopyLoadManager(context.getApplicationContext(), "city.db");
        String sql = "SELECT * FROM sl_city WHERE state = \'" + provinceto + "\' AND display = 2;";
        Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            String s1 = cursor.getString(cursor.getColumnIndex("city"));
            stringList.add(s1);
//            Log.d("Ani", s1 + "   s1");
            while (cursor.moveToNext()) {
                String s2 = cursor.getString(cursor.getColumnIndex("city"));
//                Log.d("Ani", s2 + "   s2");
                stringList.add(s2);
            }
        }
        return stringList;
    }

    /*
  * 读取区县级
  *
  * */
    public static List<String> getAreatoData(Context context, String provinceto, String cityto) {
        List<String> stringList = new ArrayList<>();
        if (sqLiteDatabase == null)
            sqLiteDatabase = DataLoad.DBCopyLoadManager(context.getApplicationContext(), "city.db");
        String sqlCode = "SELECT * FROM sl_city WHERE state = \'" + provinceto + "\' AND city =  \'" + cityto + "\' AND display = 2;";
        Cursor cursorCode = sqLiteDatabase.rawQuery(sqlCode, null);
        if (cursorCode.moveToFirst()) {
            String code = cursorCode.getString(cursorCode.getColumnIndex("sz_code"));
            Log.d("Ani",code + "  sz_code");
            String sql = "SELECT * FROM sl_city WHERE state = \'" + provinceto + "\' AND sz_code LIKE \'" + code.substring(0, code.length() - 2) + "%\'  AND display = 1;";
            Log.d("Ani",sql + "  sql");
            Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                String s1 = cursor.getString(cursor.getColumnIndex("city"));
                stringList.add(s1);
//            Log.d("Ani", s1 + "   s1");
                while (cursor.moveToNext()) {
                    String s2 = cursor.getString(cursor.getColumnIndex("city"));
//                Log.d("Ani", s2 + "   s2");
                    stringList.add(s2);
                }
            }
        }
        return stringList;
    }
}